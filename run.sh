#!/bin/sh -e

TMP_DIR=/tmp/gitbucket

mkdir ${TMP_DIR} || true

java -jar /app/gitbucket.war --port=${PORT} --gitbucket.home=/data --temp_dir=${TMP_DIR}