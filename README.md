# docker-gitbucket

Gitbucket Dockerfile

to build :
./build.sh

to run docker image :

docker run -p 8080:8080 --name gitbucket --detach -v  /tmp/gitbucket/data/:/data-v /tmp/gitbucket/tmp/:/tmp gitbucket:<version set in build.sh>