#/bin/sh -e
VERSION='4.29.0'
IMAGE_NAME=loiclacombe/gitbucket

docker build --compress -t $IMAGE_NAME:${VERSION} --build-arg GITBUCKET_VERSION=${VERSION} .
docker tag $IMAGE_NAME:${VERSION} $IMAGE_NAME:latest
docker push $IMAGE_NAME:${VERSION}
docker push $IMAGE_NAME:latest