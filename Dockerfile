FROM debian:stretch-slim

ARG DEBIAN_FRONTEND=noninteractive

ENV JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64"
ENV PORT=8080
ARG GITBUCKET_VERSION="4.29.0"
ARG DOWNLOAD_URL="https://github.com/gitbucket/gitbucket/releases/download/${GITBUCKET_VERSION}/gitbucket.war"
ARG CHECKSUM=

ARG user=gitbucket
ARG group=gitbucket
ARG uid=1000
ARG gid=1000

ARG APP_DIR=/app

RUN set -vx && groupadd -g ${gid} ${group} \
    && useradd -u ${uid} -g ${gid} --no-user-group -s /bin/nologin ${user} && \
    apt-get update && \
    mkdir -p /usr/share/man/man1/ && \
    apt-get install -y --no-install-recommends openjdk-8-jre-headless \
    curl && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    mkdir ${APP_DIR} && \
    curl ${DOWNLOAD_URL} --location -o ${APP_DIR}/gitbucket.war && \
    apt-get remove -y --purge curl

COPY run.sh /app/run.sh

VOLUME "/data"
VOLUME "/tmp"

RUN chmod ugo+x /app/run.sh /tmp && \
    chown -R gitbucket /data

USER gitbucket
EXPOSE ${PORT}
WORKDIR ${APP_DIR}

CMD ["/app/run.sh"]

